package topic03.mvc.controller;


import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import topic03.mvc.models.Book;
import topic03.mvc.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;


@Controller
public class BookController {

    private BookService bookService;

    @Autowired
    public BookController(BookService bookService){
        this.bookService=bookService;
    }

    @GetMapping({"/index", "/"})
    public String index(Model model){
        List<Book> bookList=this.bookService.getAll();
        model.addAttribute("books",bookList);
        return "mvc/book/index";
    }

    @GetMapping("/view/{id}")
    public String view(@PathVariable("id") Integer id, Model model) {
        Book book = this.bookService.findOne(id);
        model.addAttribute("book", book);
        return "mvc/book/view-detail";
    }

    @GetMapping("/update/{book_id}")
    public String showFormUpdate(@PathVariable Integer book_id, ModelMap modelMap){
         Book book =this.bookService.findOne(book_id);

         modelMap.addAttribute("isNew",false);
         modelMap.addAttribute("book",book);
        return "mvc/book/create-book";
    }

    @PostMapping("/update/submit")
    public String updateSubmit(@ModelAttribute Book book, MultipartFile file){
        System.out.println(book);

        File path=new File("/btb");

        if(!path.exists()){
            path.mkdir();
        }

        String filename=file.getOriginalFilename();

        String extension=filename.substring(filename.lastIndexOf('.') + 1);
        System.out.print(filename);
        System.out.print(extension);

        filename = UUID.randomUUID() +"."+extension;

        try{
            Files.copy(file.getInputStream(), Paths.get(("/btb"),filename));
        }catch(IOException ex){

        }

        if(!file.isEmpty()){
            book.setThumbnail("/images-btb/" + filename);
        }


        this.bookService.update(book);
        return "redirect:/index";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id){
        this.bookService.delete(id);
        return "redirect:/index";
    }

    @GetMapping("/create")
    public String create(Model model){

        model.addAttribute("isNew",true);
        model.addAttribute("book", new Book());
        return "mvc/book/create-book";
    }

    @PostMapping("/create/submit")
    public String create(@Valid Book book, BindingResult bindingResult, MultipartFile file){

        if(file==null){
            return null;
        }
        File path=new File("/btb");

        if(!path.exists()){
            path.mkdir();
        }

        String filename=file.getOriginalFilename();

        String extension=filename.substring(filename.lastIndexOf('.') + 1);
        System.out.print(filename);
        System.out.print(extension);

        filename = UUID.randomUUID() +"."+extension;

        try{
            Files.copy(file.getInputStream(), Paths.get(("/btb"),filename));
        }catch(IOException ex){

        }

        book.setThumbnail("/images-btb/"+filename);

        //To check validation
        if(bindingResult.hasErrors()){
            return "mvc/book/create-book";
        }
        System.out.print(book);
        this.bookService.create(book);
        return "redirect:/index";

    }

    @GetMapping("/test-multi-upload")
    public String showUpLoad(){
        return "mvc/book/upload-file";
    }

    @PostMapping("/test-multi-upload/submit")
    public String testMultipleFileUpload(@RequestParam("file") List<MultipartFile> files ){
        files.forEach(file -> {
            System.out.print(file.getOriginalFilename());
        });

        return "";
    }
}
