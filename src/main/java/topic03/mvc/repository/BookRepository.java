package topic03.mvc.repository;

import topic03.mvc.models.Book;
import com.github.javafaker.Faker;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class BookRepository {

    Faker faker=new Faker();

    List<Book> bookList=new ArrayList<>();

    //To write in block statement, it will run when program load
    {
        for(int i=1;i<11;i++){
            Book book=new Book();

            book.setId(i);
            book.setTitle(faker.book().title());
            book.setAuthor(faker.book().author());
            book.setPublisher(faker.book().publisher());
            bookList.add(book);
        }
    }

    //To throw data into index page
    public List<Book> getAll(){
        return this.bookList;
    }

    public Book findOne(Integer id){
        for(int i=0;i<bookList.size();i++){
            if(bookList.get(i).getId()==id){
               return bookList.get(i);
            }
        }
        return null;
    }

    public boolean update(Book book){
        for(int i=0;i<bookList.size();i++){
            if(bookList.get(i).getId()==book.getId()){
                bookList.set(i,book);
                return true;
            }
        }
        return false;
    }

    public boolean delete(Integer id){
        for(int i=0; i<bookList.size(); i++){
            if(bookList.get(i).getId() ==id){
                bookList.remove(i);
                return true;
            }
        }
        return false;
    }

    public boolean create(Book book){
        return bookList.add(book);
    }
}
